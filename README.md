# Drupal 7

Drupal is an open source content management platform supporting a variety of
websites ranging from personal weblogs to large community-driven websites. For
more information, visit the Drupal website, [Drupal.org][Drupal.org], and join
the [Drupal community][Drupal community].

This project requires that [Lando](https://lando.dev/) is installed locally on your machine.  Lando is a local development and DevOps tool trusted by professional developers.

## Installation
From inside the base directory of this repository on your local machine, simply run:

```sh
$ lando start
$ lando drupal:init
```

At this point, you can browse to http://drupal.lndo.site/.  Remember to replace references to "localhost" with "database" in order to connect the local db server.
